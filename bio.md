#drewsm - basic bio info
Who has time for hobbies? When I had a hobby, so to speak, it was hunting
edible mushrooms in the forest.  In the late spring morels come out.  In the
autumn it's chantrelles.

I have more interests than I could name, but the one that takes up most of my
time and attention is information / cyber-security.  That has been my career
path for the past 10 years.  I participate in several security-focused
organizations with meetings around the region.  I find this both intellectually
stimulating and to create opportunities for career growth.  So, I do take
a lot of personal time going to information security meetings and conferences.

#Programming Background
